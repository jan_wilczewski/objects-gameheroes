Zadanie 3 DODATKOWE:
Celem ćwiczenia jest stworzenie dwóch zespołów bohaterów, którzy mogą ze sobą walczyć
1. Stwórz enuma Side: EVIL, GOOD.

2.Stwórz klasę abstrakcyjną Hero. To będzie klasa, którą będzie rozszerzać każdy rodzaj bohatera.
-Dodaj pola klasy: String name, double HP, double attackPoints, double defPoints, Side side, Date spawnDate.
-W argumencie konstruktora podaje się imię oraz Side. Pozostałe wartości inicjalizuje się następująco:
                        -HP (1000)
                        -boolean isBoss (false)
                        -attackPoints (100)
                        -defPoints (50)
                        -spawnDate zinicjalizuj według uznania. Zrób mały research.
Dodaj gettery i settery oraz abstrakcyjną metodę doSpecialMove(Hero hero).

3. Następnym krokiem będzie utworzenie poszczególnych, konkretnych klas, które będą rozszerzać klasę Hero. Każda z poszczególnych klas ma
inne bonusy.
        a)Tank:
                -HP x 2
                -defPoints x 1,2
        b)Asassin:
                -attackPoints x 2,5
        c) Priest:
                -HP x 1.5
Metodę doSpecialMove(Hero hero) póki co zostaw pustą. ( {} ). Konstruktor powinien mieć takie same argumenty, co konstruktor nadklasy.

4.Dodaj do klasy Hero metodę attack(Hero enemy) oraz getAttacked().
        -W metodzie attack(Hero enemy) wywołuje się metodę getAttacked(int attackPoints) na obiekcie Hero enemy. W argumencie getAttacked (int attackPoints) przekazuje się punkty ataku.
        -W metodzie getAttacked(int attackPoints) odejmuje się od HP  wartość przekazanych attackPoints pomniejszoną o defPoints atakowanego bohatera
5. Kolejnym etapem będzie stworzenie areny. Będzie to klasa (Arena) instancjonowana w metodzie main(). Klasa powinna mieć dwa prywatne pola, które będą listami bohaterów:
        -List<Hero> goodGuys
        -List<Hero> badGuys
Listy mają być inicjalizowane w konstruktorze jako puste listy. Klasa Arena powinna mieć metodę addHero(Hero Hero), w której dodaje się bohatera do odpowiedniej listy, zważywszy na Side konkretnego bohatera. Metoda sama powinna sprawdzić, do jakiej strony należy dany bohater.
Najstarszy bohater zostaje bossem (weź pod uwagę pole spawnDate) i jeszcze raz dostaje bonus (taki jak w konstruktorze).

6*. Nadpisz metodę toString() w poszczególnych klasach i do klasy Arena dodaj metodę printHeroes(), która w czytelny sposób przedstawi składy dwóch zespołów, wraz z ich statystykami.

7**. Uzupełnij metody doSpecialMove() w poszczególnych konkretnych klasach postaci. Tutaj popisz się wyobraźnią. Metody mogą tu zmieniać poszczególne wartości pól klasy, zarówno wywołującego, jak i bohatera przekazywanego w argumencie funkcji (wroga, lub przyjaciela).

8***. Dodaj funkcjonalność związaną z walką między dwoma zespołami. Spróbuj wykorzystać klasę Random. Możesz dodać funkcjonalność biorącą pod uwagę to, że punkty hp bohatera spadają do lub poniżej zera. Wtedy może być usuwany z listy. Ogranicza cię tylko wyobraźnia i Maszyna Wirtualna Javy!!
