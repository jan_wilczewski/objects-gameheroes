import java.util.List;
import java.util.Random;

/**
 * Created by jan_w on 20.09.2017.
 */
public class Main {

    public static Arena arena = new Arena();

    public static void main(String[] args) {

        arena.addHero(new Assasin("Predator", eSide.EVIL));
        arena.addHero(new Assasin("Alien", eSide.EVIL));
        arena.addHero(new Assasin("Godzilla", eSide.EVIL));

        arena.addHero(new Tank("Rudy 102", eSide.GOOD));
        arena.addHero(new Assasin("Rambo", eSide.GOOD));
        arena.addHero(new Priest("Pastor", eSide.GOOD));

        arena.printHeroes();

        arena.fight(arena.getGoodGuys(), arena.getBadGuys());

    }
}