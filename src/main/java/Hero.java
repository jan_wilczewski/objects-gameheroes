import java.sql.Date;

/**
 * Created by jan_w on 19.09.2017.
 */
public abstract class Hero {

    public RandomDate randomDate = new RandomDate();

    public String name;
    public double HP;
    public double attackPoints;
    public double defPoints;
    public eSide side;
    public Date spawnDate;

    public Hero(String name, eSide side) {
        this.name = name;
        this.HP = 1000;
        this.attackPoints = 100;
        this.defPoints = 50;
        this.side = side;
        this.spawnDate = Date.valueOf(randomDate.randomBirthday());
    }

    public Hero() {
    }

    public abstract void doSpecialMove(Hero hero);

    public void attack (Hero enemy){
        enemy.getAttacked((int) attackPoints);
    }

    public void getAttacked (int attackPoints){
        double toRemove =attackPoints - defPoints;
        if(toRemove <= 10){
            toRemove = 10;
        }
        setHP(HP -toRemove);
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHP() {
        return HP;
    }

    public void setHP(double HP) {
        this.HP = HP;
    }

    public double getAttackPoints() {
        return attackPoints;
    }

    public void setAttackPoints(double attackPoints) {
        this.attackPoints = attackPoints;
    }

    public double getDefPoints() {
        return defPoints;
    }

    public void setDefPoints(double defPoints) {
        this.defPoints = defPoints;
    }

    public eSide getSide() {
        return side;
    }

    public void setSide(eSide side) {
        this.side = side;
    }

    public Date getSpawnDate() {
        return spawnDate;
    }

    public void setSpawnDate(Date spawnDate) {
        this.spawnDate = spawnDate;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", HP=" + HP +
                ", attackPoints=" + attackPoints +
                ", defPoints=" + defPoints +
                ", side=" + side +
                ", spawnDate=" + spawnDate +
                '}';
    }
}
