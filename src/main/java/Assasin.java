import java.sql.Date;

/**
 * Created by jan_w on 20.09.2017.
 */
public class Assasin extends Hero {


    public Assasin(String name, eSide side) {
        super();
        this.name = name;
        this.HP = 1000;
        this.attackPoints = 100 *2.5;
        this.defPoints = 50;
        this.side = side;
        this.spawnDate = Date.valueOf(randomDate.randomBirthday());
    }

    @Override
    public void doSpecialMove(Hero hero) {
        hero.setDefPoints(hero.getDefPoints() * 0.75);
    }

    @Override
    public String toString() {
        return "Assasin{" +
                "name='" + name + '\'' +
                ", HP=" + HP +
                ", attackPoints=" + attackPoints +
                ", defPoints=" + defPoints +
                ", side=" + side +
                ", spawnDate=" + spawnDate +
                '}';
    }
}
