import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jan_w on 20.09.2017.
 */
public class Arena {

    private List<Hero> goodGuys = new ArrayList<>();
    private List<Hero> badGuys = new ArrayList<>();

    public Arena(List<Hero> goodGuys, List<Hero> badGuys) {
        this.goodGuys = new ArrayList<>();
        this.badGuys = new ArrayList<>();
    }

    public Arena() {
    }

    public void addHero (Hero hero){
        if (hero.getSide().equals(eSide.GOOD)){
            goodGuys.add(hero);
        }else {
            badGuys.add(hero);
        }
    }

    public void printHeroes(){
        System.out.println("Drużyna GOOD:");
        for (Hero good: goodGuys){
            System.out.println(good);
        }
        System.out.println();
        System.out.println("Drużyna EVILLLL:");
        for (Hero bad: badGuys){
            System.out.println(bad);
        }
    }



    public void fight(List<Hero> goodGuys, List<Hero> badGuys){

        int runda = 0;

        boolean isRunning = true;

        do {
            Random r = new Random();
            int randG = r.nextInt(getGoodGuys().size());
            int randB = r.nextInt(getBadGuys().size());
            Random s = new Random();
            boolean spec = s.nextBoolean();
            Random t = new Random();
            int turn = t.nextInt(2)+1;
            System.out.println("Runda: " + runda);
            printHeroes();

            if (turn == 1){
                getGoodGuys().get(randG).attack(getBadGuys().get(randB));
                if (spec = true){
                    getGoodGuys().get(randG).doSpecialMove(getGoodGuys().get(randG));
                }
            }

            else if (turn == 2 ){
                getBadGuys().get(randB).attack(getGoodGuys().get(randB));
                if (spec = true){
                    getBadGuys().get(randB).doSpecialMove(getGoodGuys().get(randG));
                }
            }

            runda++;

            for (Hero good: goodGuys){
                if (good.getHP() < 0){
                    isRunning = false;
                }
            }

            for (Hero bad: badGuys){
                if (bad.getHP() < 0){
                    isRunning = false;
                }
            }

        }while (isRunning);
    }






    public List<Hero> getGoodGuys() {
        return goodGuys;
    }

    public void setGoodGuys(List<Hero> goodGuys) {
        this.goodGuys = goodGuys;
    }

    public List<Hero> getBadGuys() {
        return badGuys;
    }

    public void setBadGuys(List<Hero> badGuys) {
        this.badGuys = badGuys;
    }
}
