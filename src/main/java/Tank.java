import java.sql.Date;

/**
 * Created by jan_w on 20.09.2017.
 */
public class Tank extends Hero{


    public Tank(String name, eSide side) {
        super();
        this.name = name;
        this.HP = 1000 * 2;
        this.attackPoints = 100;
        this.defPoints = 50 * 1.2;
        this.side = side;
        this.spawnDate = Date.valueOf(randomDate.randomBirthday());
    }

    @Override
    public void doSpecialMove(Hero hero) {
        hero.setAttackPoints(hero.getAttackPoints() * 1.5);
    }

    @Override
    public String toString() {
        return "Tank{" +
                "name='" + name + '\'' +
                ", HP=" + HP +
                ", attackPoints=" + attackPoints +
                ", defPoints=" + defPoints +
                ", side=" + side +
                ", spawnDate=" + spawnDate +
                '}';
    }
}
