import java.sql.Date;

/**
 * Created by jan_w on 20.09.2017.
 */
public class Priest extends Hero {


    public Priest(String name, eSide side) {
        super();
        this.name = name;
        this.HP = 1000 * 1.5;
        this.attackPoints = 100;
        this.defPoints = 50;
        this.side = side;
        this.spawnDate = Date.valueOf(randomDate.randomBirthday());
    }

    @Override
    public void doSpecialMove(Hero hero) {
        hero.setHP(hero.getHP() * 2);
    }

    @Override
    public String toString() {
        return "Priest{" +
                "name='" + name + '\'' +
                ", HP=" + HP +
                ", attackPoints=" + attackPoints +
                ", defPoints=" + defPoints +
                ", side=" + side +
                ", spawnDate=" + spawnDate +
                '}';
    }
}
